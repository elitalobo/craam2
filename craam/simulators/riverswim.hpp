//
//  riverswim.hpp
//  CRAAM
//
//  Created by Elita  Lobo on 5/12/20.
//

#ifndef riverswim_h
#define riverswim_h


#endif /* riverswim_h */
#pragma once

#include "craam/Samples.hpp"
#include "craam/definitions.hpp"

#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <memory>
#include <random>
#include <string>
#include <utility>
#include <vector>

namespace craam { namespace msen {

using namespace std;
using namespace util::lang;




 * A simple riverswim model simulator
 *
 *
 * State 0 represents initial position of swimmer
 */
class Transition(){
        public:
            using state = long;
            using action = long;
            using next_state = long;
            prec_t prob;
            prec_t reward;
            
            Transition(long state_ long action_, long next_state_, double prob_, double reward_){
                state = state_;
                action = action_;
                next_state = next_state_;
                prob = prob_;
                reward = reward_;
                
            }
};
            
class Transitions(){
public:
    vector<Transition> transitions;
    long num_states;
    long num_actions;
    Transitions(long n_states, long n_actions){
        num_states = n_states;
        num_actions = n_actions;
    }
    
    void add(long state, long action, long next_state, prec_t prob, prec_t reward){
        Transition transition(state, action, next_state, prob, reward);
        transitions.push_back(transition);
    }
    
    vector<Transition> fetch_all(){
        return transitions;
    }
    
    size_t length(){
        return transitions.size();
    }
    
    double*** get_transition_matrix(){
        double*** transition_matrix = (double***)malloc(num_states*sizeof(double**));
        for(int i=0;i<num_states;i++){
            transition_matrix[i] = (double**)malloc(num_actions*sizeof(double*));
            for(int j=0;j<num_actions;j++){
                transition_matrix[i][j]= (double*)malloc(num_states*sizeof(double));
            }
        
        }
        for(int idx=0;idx<transitions.size();idx++){
            transition_matrix[transitions[idx].state,transitions[idx].action,transitions[idx].next_state]=transition[idx].prob;
        }
        return transition_matrix;
        
    }
    
    double*** get_reward_matrix(){
        double*** reward_matrix = (double***)malloc(num_states*sizeof(double**));
        for(int i=0;i<num_states;i++){
            reward_matrix[i] = (double**)malloc(num_actions*sizeof(double*));
            for(int j=0;j<num_actions;j++){
                reward_matrix[i][j]= (double*)malloc(num_states*sizeof(double));
            }
        
        }
        for(int idx=0;idx<transitions.size();idx++){
            reward_matrix[transitions[idx].state,transitions[idx].action,transitions[idx].next_state]=transition[idx].reward;
        }
        
        return reward_matrix;
        
    }
    
};
    
   
   


    

                
    

                            
                            
class RiverswimSimulator {

public:
    /// Type of states: swimmer location
    using State = long;
    /// Type of actions: direction of swim
    using Action = long;
    
    int num_states;
    int num_actions;
    const numvecvecvec rewards;
    const double*** transition_matrix;
    const numvecvecvec prior;
    
   
    RiverswimSimulator(int n_states, int n_actions)
    {
        num_states = n_states;
        num_actions = n_actions;
        for(int i=0;i<num_states,i++){
            const numvecvec a;
            const numvecvec pa;
            for(int j=0;j<num_actions;j++){
                const numvec b;
                const numvec pb;
                for(int k=0;k<num_states;k++){
                    if(k==0){
                        b.push_back(5.0/1000);
                    }
                    else if(k==5){
                        b.push_back(1.0);
                    }
                    else
                    b.push_back(0);
                    pb.push_back(0);
                    
                }
                a.push_back(b);
                pb[i]=1
                pb[min(num_states-1,i+1)]=1
                pb[max(0,i-1)]=1
                pa.push_back(pb);
            }
            rewards.push_back(a);
            prior.push_back(pa);
        }
        construct_mdp();
    }
        
        void construct_mdp(prec_t discount_factor=0.99){
            Transitions transitions(num_states, num_actions);
            for(int state=0;state<num_states;state++){
                transitions.add(state,0,max(state-1,0),1,rewards[state,0,max(state-1,0)]);
                if(state==0){
                    transitions.add(state, 1, min(state + 1, num_states - 1),0.6, rewards[state, 1, min(state + 1, num_states - 1)]);
                    transitions.add(state, 1, max(state - 1, 0),0.4, rewards[state, 1, max(state - 1, 0)]);
                    transitions.add(state,1,state,0.4, rewards[state,1,state]);

                }
                else if(state == num_states-1) {
                    transitions.add(state, 1, min(state + 1, num_states - 1), 0.6, rewards[state, 1,min(state + 1, num_states - 1) ]);
                    transitions.add(state, 1, max(state - 1, 0),0.4,rewards[state, 1, max(state - 1, 0)]);
                    transitions.add(state, 1, state, 0.6, rewards[state, 1, state]);

                }

                else{
                    transitions.add(state, 1, min(state + 1, self.num_states - 1), 0.35, self.rewards[state, 1, min(state + 1, self.num_states - 1)])
                    transitions.add(state, 1, max(state - 1, 0),0.05,self.rewards[state, 1, max(state - 1, 0)])

                    transitions.add(state, 1, state, 0.6, self.rewards[state, 1, state])
                }

            }
            transition_matrix = transitions.get_transition_matrix();
            
        }
        
    double*** get_transition_matrix(){
        return transitions.get_transition_matrix();
    }
        

       
        

        
    


    State init_state() const { return 0; }

    
    vector<long> sample_integers(numvec weights, vec<int> vals, int num_samples ){
        
        std::discrete_distribution<int> dist(std::begin(weights), std::end(weights));
        std::mt19937 gen;
        gen.seed(time(0));
        
        std::vector<int> samples(num_samples);
        for(auto & i: samples){
            i = dist(gen);
            
        }
        return samples;
    }
        
    pair<prec_t, State> transition(State current_state, Action action) noexcept {
        
        pair<prec_t,State> res;
        numvec weights ;
        vec<long> vals;
        for(int i=0;i<num_states;i++){
            weights.push_back(transition_matrix[state][action][i]);
            vals.push_back(i);
        }
        vector<long> samples = sample_integers(weights,vals);
        
        return std::make_pair(rewards[current_state][action][sample[0],samples[0]);
    }

    

    /// Returns the number of states
    long state_count() const noexcept { return max_inventory + 1 + max_backlog; }

    /// Returns the number of actions
    long action_count() const noexcept { return max_order; }

    /// Returns an element that supports size and []
    LightArray get_valid_actions(State s) const noexcept {
        return LightArray(action_count());
    }

    /**
     * Calls the function F for all transition probabilities. Can be used to
     * construct an MDP with the transition probabilities that correspond to
     * this inventory management problem.
     *
     * @tparam F A type that can be called as
     * (long fromid, long actionid, long toid, prec_t probability, prec_t reward)
     *
     * @param f MDP construction function
     */
    template <class F> void build_mdp(F&& f) const {
        for (State statefrom = 0; statefrom < state_count(); ++statefrom) {
            for (Action action = 0; action < action_count(); ++action) {
                
                    State stateto;
                    prec_t reward;
                    // simulate a single step of the transition probabilities
                    std::tie(reward, stateto) = transition(statefrom, action);
                    const prec_t probability = transition[statefrom][action][stateto];
                    // run the method that add the transition probability
                    // and possibly update progress
                    f(statefrom, action, stateto, probability, reward);
                
            }
        }
    }

    //template <class method> generate_model() {}


};

///Inventory policy to be used
using ModelInventoryPolicy = ThresholdPolicy<InventorySimulator>;

}} // namespace craam::msen

